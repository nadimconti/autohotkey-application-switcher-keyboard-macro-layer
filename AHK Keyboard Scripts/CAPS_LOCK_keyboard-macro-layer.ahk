;; --- THIS IS A SCRIPT TO MAP KEYS ON a standard keyboard to switch apps "and do other things ----
;; --- and we chosed to do this, not becouse those things are easy, but because they are hard" JFK         

;; --- Keys will be activated on CAPS LOCK ACTIVE !!!
;; --- therefore, the whole keyboard will act like a second keyboard (MACRO) while the CAPS LOCK is ON, and normally when
;; --- CAPS LOCK is OFF.

;; note that the script does not sense the lock state, but only the toggle one, therefore any key can be used instead of the caps lock
;; as the lock state is not sensed, no lights will be activated when the caps lock is on (I.E MACRO LAYER ACTIVE).

;; I prefer to have something on my screen that can clearly tell me "DO NOT START WRITING SOMETHING", as if you start with the macro layer on,
;; you might end up starting the boot sequence of 5-12 apps in a few seconds without even realizing.

;; ----------------------------------------------------------------------------------------------------------------------

;; DEVELOPED FROM:
;; 	https://github.com/TaranVH/2nd-keyboard/blob/9ee39aaccf9c58df9c4a7c19eb7d12ab5223872c/ALL_MULTIPLE_KEYBOARD_ASSIGNMENTS.ahk#L1253

;; AND FROM:
;; 	https://github.com/TaranVH/2nd-keyboard/blob/9ee39aaccf9c58df9c4a7c19eb7d12ab5223872c/Almost_All_Windows_Functions.ahk#L570

;; AND FROM (only for locking the caps lock state)
;;  https://www.autohotkey.com/docs/commands/GetKeyState.htm

;; GIF ONLINE RESIZE SERVICE: https://ezgif.com/resize.

;; ----------------------------------------------------------------------------------------------------------------------

;#NoTrayIcon
#SingleInstance ;; forces only a single instance of the program to be running so that we avoid a real mess happening when pushing buttons

State = 0 ;; initialization of the state variable to know if the macro layer should be on or not

CapsLock::
Suspend, permit ;; suspending the code or resuming it on the press of caps lock

if (State = 0) {
	;;Gui, Destroy ; Show, Hide ;; OTHER Method, instead of moving the image, we could delete it, but it's slow
	
	;; Here we move the image out of the screen so that it doesn't signal us that the macro is on
	Gui Color, White  ;; defines the color of the GUI, necessary to make it transparent later
	Gui -caption +toolwindow +AlwaysOnTop  ;; keeps the image always on top
	;Gui font, s30 bold, Arial ;; Defines text font and dimension, comment if picture is used and no text
	;Gui add, text, vTX cRed TransColor, MACRO ON   ;; Adds text on the scree, color RED, comment if picture is used and no text
	pic := "C:\Users\NC\Documents\Auto Hotkey Scripts\nyan-cat.gif" ;; defines the path of the picture to show, comment if text is used and no pic
	Gui, Add, ActiveX, w150 h100, % "mshtml:<img src='" pic "' />" ;; necessary to animate the picture in case it it a GIF, comment if text is used and no pic
	Gui Show, % "x" A_ScreenWidth-2000 " y" A_ScreenHeight-0, TRANS-WIN ;; Shows the window, it makes it visible in a certain location of the screen, window is transparent, OUT OF SCREEN, one might need to tweak the numbers here
	WinSet TransColor, White, TRANS-WIN ;; sets all the pixels that are WHITE to transparent, even when we do not want to write anything there
	State := 1

} else {
	State := 0
	;; Here we move the image INSIDE of the screen so that it doesn't signal us that the macro is on
	Gui Color, White  ;; defines the color of the GUI, necessary to make it transparent later
	Gui -caption +toolwindow +AlwaysOnTop  ;; keeps the image always on top
	;Gui font, s30 bold, Arial ;; Defines text font and dimension, comment if picture is used and no text
	;Gui add, text, vTX cRed TransColor, MACRO ON   ;; Adds text on the scree, color RED, comment if picture is used and no text
	pic := "C:\Users\NC\Documents\Auto Hotkey Scripts\nyan-cat.gif" ;; defines the path of the picture to show, comment if text is used and no pic
	Gui, Add, ActiveX, w150 h100, % "mshtml:<img src='" pic "' />" ;; necessary to animate the picture in case it it a GIF, comment if text is used and no pic
	Gui Show, % "x" A_ScreenWidth-200 " y" A_ScreenHeight-130, TRANS-WIN ;; Shows the window, it makes it visible in a certain location of the screen, window is transparent
	WinSet TransColor, White, TRANS-WIN ;; sets all the pixels that are WHITE to transparent, even when we do not want to write anything there
}
Suspend, toggle
return




if (State = 1) 
	
{
		


		
		;;---HERE WE MAP THE KEY PRESSED TO THE FUNCTION THAT WILL ACTUALLY EXECUTE THE ACTION ---
	
		;#IfWinActive
	
		;; KEEP IN MIND: <<<<<<<<<<<<<<<<<<<<<
		;; MACRO KEYS ARE SPECIFIED IN COLOUMNS (therefore not as QWERTY, but as QAZ,WSX,EDC,RFV... as the distance from left hand grows)
		;; But first, the numbers, easiest to reach (In my experience).
		
		;macro key \ 
		; This will trigger the "CLOSE ACTION"
		;\::^w
		
		;macro key 1 (THE NUMBER)
		; This will trigger the "BACK ACTION" (see ctrl-z, kind of depending on the application that's active)
		1::^z
		
		;macro key 2 (THE NUMBER)
		; This will trigger the "FORWARD ACTION" (see CTRL-y, kind of as with previous)
		2::^y
		
		;macro key 3 (THE NUMBER)
		; This will trigger the "CUT"
		;3::^x
		
		;macro key 4 (THE NUMBER)
		; This will trigger the "COPY"
		;4::^c
		
		;macro key 5 (THE NUMBER)
		; This will trigger the "PASTE"
		;5::^v
		
		;macro key 6 (THE NUMBER)
		; This will trigger the "SELECT ALL"
		;6::^a
		
		;macro key 7 (THE NUMBER)
		; This will trigger the "SAVE"
		;7::^s
		
		;-------------------------------------------------------- NEW KEYBOARD COLOUMN -------------------------------------
		; Macro key <
		<::switchToFreeCAD()
		
		;-------------------------------------------------------- NEW KEYBOARD COLOUMN -------------------------------------
		
		;Macro key Q
		; This will start or activate the application (can be customized of course): QUCS Simulator
		Q::switchToQUCS()
		+Q::goBackOneTabQUCS()
		^Q::closeTabQUCS()
		
		;macro key A
		; This will start or activate the application (can be customized of course): ALTIUM DESIGNER
		A::switchToAltiumDesigner()
		+A::goBackOneTabAltiumDesigner()
		
		;macro key Z
		; This will start or activate the application (can be customized of course):  SATURN PCB TOOLKIT
		Z::switchToSaturnPCB()
		
		;-------------------------------------------------------- NEW KEYBOARD COLOUMN -------------------------------------

		;macro key W
		; This will start or activate the application (can be customized of course): WORD PROCESSOR
		W::switchToWord()
		
		;macro key S
		; This will start or activate the application (can be customized of course): Speed Crunch (calculator)
		S::switchToSpeedCrunch()
		
		;macro key X
		; This will start or activate the application (can be customized of course): VLC mediaplayer
		X::switchToVLC()
		
		;-------------------------------------------------------- NEW KEYBOARD COLOUMN -------------------------------------
		
		;macro key E
		; This will start or activate the application (can be customized of course): EXEL
		E::switchToExcel()

		;macro key D
		; This will start or activate the application (can be customized of course): FILE EXPLORER (WINDOWS)
		D::switchToExplorer()
		!D::closeAllExplorers()

		;macro key C
		; This will start or activate the application (can be customized of course): CHROME WEB BROWSER
		C::switchToChrome()
		+C::goBackOneTabChrome()
		^C::openLastClosedTabChrome()
		!C::closeCurrentTabChrome()
		
		;-------------------------------------------------------- NEW KEYBOARD COLOUMN -------------------------------------
	
		;macro key R
		; This will start or activate the application (can be customized of course):  OBS screen recorder
		R::switchToOBS()

		;macro key F
		; This will start or activate the application (can be customized of course): Everything file searcher
		F::switchToEverything()
		
		;macro key V
		; This will start or activate the application (can be customized of course): THUNDERBIRD (EMAIL)
		V::switchToThunderbird()
		+V::goBackOneTabThunderbird()
		^V::openLastClosedTabThunderbird()
		!V::closeCurrentTabThunderbird()
		
		;-------------------------------------------------------- NEW KEYBOARD COLOUMN -------------------------------------
		
		;macro key T
		; This will start or activate the application (can be customized of course): Trello
		T::switchToTrello()
		+T::goBackOneBoardTrello()
		
		;macro key G
		; This will start or activate the application (can be customized of course): TELEGRAM
		G::switchToTelegram()
		+G::goBackOneChatTelegram()
		
		;macro key B
		; This will start or activate the application (can be customized of course):
		
		;-------------------------------------------------------- NEW KEYBOARD COLOUMN -------------------------------------
		
		;macro key Y
		; This will start or activate the application (can be customized of course): SLACK
		Y::switchToSlack()

		;;------END OF CTRL FUNCITON KEYS----------
}


;; BEGINNING OF FUNCTIONS TO CALL AND SWITCH APPLICATIONS WITH ONE KEY (the order here might be not very... in order... but apps are separated!

; ------------------------------------------------------------------------------------------ OBS Recorder ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<

switchToOBS()
{
IfWinNotExist, ahk_exe obs64.exe ;; if app is not open, open it
	Run obs64.exe, C:\Program Files (x86)\obs-studio\bin\64bit\
;if WinActive("ahk_exe obs64.exe") ;; if app is open and active, switch between tabs
;	Sendinput ^{tab}
else
	WinActivate ahk_exe obs64.exe ;; if app is open, but not active, activate it on the last active view
}


; ------------------------------------------------------------------------------------------ QUCS SIMULATOR ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<

switchToQUCS()
{
IfWinNotExist, ahk_exe qucs.exe ;; if app is not open, open it
	Run qucs.exe, C:\QUCS-19\qucs-0.0.19-rc3-win64\qucs-win64\bin\
if WinActive("ahk_exe qucs.exe") ;; if app is open and active, switch between tabs
	Sendinput ^{tab}
else
	WinActivate ahk_exe qucs.exe ;; if app is open, but not active, activate it on the last active view
}

goBackOneTabQUCS()
{
if WinActive("ahk_exe qucs.exe") ;; if app is open and active, switch between tabs, but reverse direction as in "switchToChrome()"
	Sendinput ^+{tab}
}

closeTabQUCS()
{
if WinActive("ahk_exe qucs.exe") ;; if app is open and active, close the active tab
	Sendinput ^W
}


; ------------------------------------------------------------------------------------------ CHROME WEB BROWSER ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<
switchToChrome()
{
IfWinNotExist, ahk_exe chrome.exe ;; if chrome is not open, open it
	Run, chrome.exe

if WinActive("ahk_exe chrome.exe") ;; if chrome is open and active, switch between tabs
	Sendinput ^{tab}
else
	WinActivate ahk_exe chrome.exe ;; if chrome is open, but not active, activate it on the last active tab
}

goBackOneTabChrome()
{
if WinActive("ahk_exe chrome.exe") ;; if chrome is open and active, switch between tabs, but reverse direction as in "switchToChrome()"
	Sendinput ^+{tab}
}

openLastClosedTabChrome()
{
if WinActive("ahk_exe chrome.exe") ;; if chrome is open and active, opens the last closed tab
	Sendinput ^+T
}

closeCurrentTabChrome()
{
if WinActive("ahk_exe chrome.exe") ;; if chrome is open and active, closes the current tab
	Sendinput ^W
}

; ------------------------------------------------------------------------------------------ THUDERBIRD EMAIL CLIENT ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<
switchToThunderbird()
{
IfWinNotExist, ahk_exe thunderbird.exe ;; if app is not open, open it
	Run thunderbird.exe, C:\Program Files (x86)\Mozilla Thunderbird\
if WinActive("ahk_exe thunderbird.exe") ;; if app is open and active, switch between tabs
	Sendinput ^{tab}
else
	WinActivate ahk_exe thunderbird.exe ;; if app is open, but not active, activate it on the last active view
}

goBackOneTabThunderbird()
{
if WinActive("ahk_exe thunderbird.exe") ;; if app is open and active, switch between tabs, but reverse direction as in "switchToChrome()"
	Sendinput ^+{tab}
}

closeCurrentTabThunderbird()
{
if WinActive("ahk_exe thunderbird.exe") ;; if app is open and active, closes the current tab .... same as in other apps, one could just join these things under only one key...
	Sendinput ^W
}

openLastClosedTabThunderbird()
{
if WinActive("ahk_exe thunderbird.exe") ;; if app is open and active, opens the last closed tab .... same as in other apps, one could just join these things under only one key...
	Sendinput ^+T
}

; ------------------------------------------------------------------------------------------ ALTIUM DESIGNER CAD ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<
switchToAltiumDesigner()
{
IfWinNotExist, ahk_exe  X2.EXE ;; if app is not open, open it
	Run X2.EXE, C:\Program Files\Altium\AD18\
if WinActive("ahk_exe X2.exe") ;; if app is open and active, switch between tabs
	Sendinput ^{tab}
else
	WinActivate ahk_exe X2.exe ;; if app is open, but not active, activate it on the last active view
}
goBackOneTabAltiumDesigner()
{
if WinActive("ahk_exe X2.exe") ;; if app is open and active, switch between tabs, but reverse direction as in "switchToChrome()"
	Sendinput ^+{tab}
}

; ------------------------------------------------------------------------------------------ WORD PROCESSOR MICROSOFT ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<
switchToWord()
{
Process, Exist, WINWORD.EXE
;msgbox errorLevel `n%errorLevel%
	If errorLevel = 0
		Run, WINWORD.EXE
	else
	{
	GroupAdd, documentwords, ahk_class OpusApp  ;; TO Switch from window to window of word is a little tricky, we need to create a group
	if WinActive("ahk_class OpusApp")
		GroupActivate, documentwords, r
	else
		WinActivate ahk_class OpusApp
	}
}

; ------------------------------------------------------------------------------------------ Speed Crunch calculator ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<

switchToSpeedCrunch()
{
IfWinNotExist, ahk_exe  speedcrunch.exe ;; if app is not open, open it
	Run speedcrunch.exe, C:\Program Files (x86)\SpeedCrunch\
; if WinActive("ahk_exe speedcrunch.exe") ;; if app is open and active, switch between tabs
;	Sendinput ^{tab}
else
	WinActivate ahk_exe speedcrunch.exe ;; if app is open, but not active, activate it on the last active view
}

; ------------------------------------------------------------------------------------------ TRELLO MANAGEMENT ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<

switchToTrello()
{
IfWinNotExist, ahk_exe  Trello.exe ;; if app is not open, open it
	Run, C:\Users\NC\Desktop\Trello
if WinActive("ahk_exe Trello.exe") ;; if app is open and active, switch between BOARDS, go to next board
	Sendinput ^{vkDB} ; vkDB is the code for [
else
	WinActivate ahk_exe Trello.exe ;; if app is open, but not active, activate it on the last active view
}
goBackOneBoardTrello()
{
if WinActive("ahk_exe Trello.exe") ;; if app is open and active, switch between tabs, but reverse direction as in "switchToChrome()"
	Sendinput ^{VKDD} ; vkdd is the code for ]
}

; ------------------------------------------------------------------------------------------ EVERYTHING FILE SEARCH ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<
switchToEverything()
{
IfWinNotExist, ahk_exe  Everything.exe ;; if app is not open, open it
	Run Everything.exe, C:\Program Files\Everything\
else
	WinActivate ahk_exe Everything.exe ;; if app is open, but not active, activate it on the last active view
}

; ------------------------------------------------------------------------------------------ FILE EXPLORER ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<

switchToExplorer(){
IfWinNotExist, ahk_class CabinetWClass
	Run, explorer.exe
	GroupAdd, openexplorers, ahk_class CabinetWClass
if WinActive("ahk_exe explorer.exe")
	GroupActivate, openexplorers, r
else
	WinActivate ahk_class CabinetWClass ;you have to use WinActivatebottom if you didn't create a window group.
}

; ;trying to activate these windows in reverse order from the above. it does not work.
; ^+F2::
; IfWinNotExist, ahk_class CabinetWClass
	; Run, explorer.exe
; GroupAdd, taranexplorers, ahk_class CabinetWClass
; if WinActive("ahk_exe explorer.exe")
	; GroupActivate, taranexplorers ;but NOT most recent.
; else
	; WinActivatebottom ahk_class CabinetWClass ;you have to use WinActivatebottom if you didn't create a window group.
; Return

;closes all explorer windows :/
;^!F2 -- for searchability

closeAllExplorers()
{
	WinClose,ahk_group openexplorers ; closing all file explorers windows that are open
}

; ------------------------------------------------------------------------------------------ TELEGRAM MESSAGING ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<
switchToTelegram()
{
IfWinNotExist, ahk_exe  Telegram.exe ;; if app is not open, open it
	Run Telegram.exe, C:\Users\NC\AppData\Roaming\Telegram Desktop\
if WinActive("ahk_exe Telegram.exe") ;; if app is open and active, switch between chats (next one)
	Sendinput ^{tab}
else
	WinActivate ahk_exe Telegram.exe ;; if app is open, but not active, activate it on the last active view
}

goBackOneChatTelegram()
{
if WinActive("ahk_exe Telegram.exe") ;; if app is open and active, switch between tabs, but reverse direction as in "switchToChrome()"
	Sendinput ^+{tab}
}
; ------------------------------------------------------------------------------------------ EXCEL ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<

switchToExcel()
{
Process, Exist, EXCEL.EXE
;msgbox errorLevel `n%errorLevel%
	If errorLevel = 0
		Run, EXCEL.EXE
	else
	{
	GroupAdd, documentexcel, ahk_class XLMAIN  ;; TO Switch from window to window of excel is a little tricky, we need to create a group
	if WinActive("ahk_class XLMAIN")
		GroupActivate, documentexcel, r
	else
		WinActivate ahk_class XLMAIN
	}
}

; ------------------------------------------------------------------------------------------ VLC Mediaplayer ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<
switchToVLC()
{
IfWinNotExist, ahk_exe  vlc.exe ;; if app is not open, open it
	Run vlc.exe, C:\Program Files\VideoLAN\VLC\
; if WinActive("ahk_exe speedcrunch.exe") ;; if app is open and active, switch between tabs
;	Sendinput ^{tab}
else
	WinActivate ahk_exe vlc.exe ;; if app is open, but not active, activate it on the last active view
}

; ------------------------------------------------------------------------------------------ SATURN PCB Toolkit ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<
switchToSaturnPCB()
{
IfWinNotExist, ahk_exe  PCBToolkitV7.08.exe ;; if app is not open, open it
	Run PCBToolkitV7.08.exe, C:\SaturnPCBDesign\PCBToolkitV7\
else
	WinActivate ahk_exe PCBToolkitV7.08.exe ;; if app is open, but not active, activate it on the last active view
}

; ------------------------------------------------------------------------------------------ SLACK ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<
switchToSlack()
{
	IfWinNotExist, ahk_exe  slack.exe ;; if app is not open, open it
	Run slack.exe, C:\Users\NC\AppData\Local\slack\
; if WinActive("ahk_exe slack.exe") ;; if app is open and active, do something useful
;	Sendinput ^{tab}
else
	WinActivate ahk_exe slack.exe ;; if app is open, but not active, activate it on the last active view
}

; ------------------------------------------------------------------------------------------ FreeCAD Design ACTIVITIES <<<<<<<<<<<<<<<<<<<<<<<<<
switchToFreeCAD()
{
	IfWinNotExist, ahk_exe  FreeCAD.exe ;; if app is not open, open it
	Run FreeCAD.exe, C:\Program Files\FreeCAD 0.18\bin\
; if WinActive("ahk_exe FreeCAD.exe") ;; if app is open and active, do something useful
;	Sendinput ^{tab}
else
	WinActivate ahk_exe FreeCAD.exe ;; if app is open, but not active, activate it on the last active view
}
