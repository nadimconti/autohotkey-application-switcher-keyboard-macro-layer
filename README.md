# AutoHotKey Application Switcher - Keyboard Macro Layer

<img src='Finished-Keyboard-picture.jpg' height="300"></img>

**What is this project for?**

This project creates a macro layer on your computer keyboard to enable fast application launch and swith to save time when performing these operations.
The macro layer is activated to the press of the CAPS-LOCK button, and deactivated in the same way.

NOTE: To use the CAPS LOCK it is possible to activate and deactivate it (its original function) by pressing SHIFT+CAPSLOCK. This will enable the "all caps" mode, or disable it, without triggering the Macro layer to turn on.

Full article on my website is [HERE](https://nadimconti.com/2020/04/02/saving-time-with-a-keyboard-macro-layer/)

The goal is achieved via an AutoHotKey script with the following specs:
 - Does Application switch
 - Does Application launch
 - Shows an anymated GIF image on the screen when Macro Layer is active
 - Activates the Macro Keyboard Layer on caps lock press


In this repo you will find: 
- my AHK code
- sources that I've used to write it
- the GIF I'm using
- a PDF with the key prints seen on the picture above (attached with double side tape and coated with a plastic clear tape)
- the word document I've used to create the PDF with all the icons inside


<!-- wp:paragraph -->
<p>Flow chart of the AHK code in a nutshell:</p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><li>Setting a few runtime variables and putting the code on stand by<br></li>
<li>Monitoring caps lock to be pressed or not<br></li>
<li>If capslock is pressed, then the code leaves stand by, the "state" variable toggles, and an animated gif is shown on the bottom right corner of my screen to let me know that the macro layer is active.<br><br><em><strong>To know that the macro layer is active is very important,</strong> why? Imagine you activate the macro layer whithout realizing, then, attracted by the search bar on google, you start typing a web address.<strong> If you're fast enough, in 2 seconds you will trigger the boot process of up to tens of apps</strong>, depending on how many keys are mapped.</em><br></li>
<li>While the macro layer is active, apps can be switched, their tabs navigated, and any further action executed.<br></li>
<li>If capslock is pressed again, "state" toggles and, the animated gif disappears, and the script goes to sleep.</li></ol>
<!-- /wp:list -->